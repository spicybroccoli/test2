<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_test2');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OadxE])+WQ[?Hly9?ho B*-W?oMW-4Lm^jtX }~6^dK{]7jp3LAV4QCMO)B4I!~`');
define('SECURE_AUTH_KEY',  'YtA0k~yjwfuf]!B,;#V^#>}9S6Ljhy~5fQ/:Y`?@8jvmeVV$SG_XQGa?9)]W9nO;');
define('LOGGED_IN_KEY',    'w&i5[+z.N+*tW^*Gz7(QX[?6Xr;oPp7spg6DNGVy[XZK{-1w=ZADu5{MWV<lS_3A');
define('NONCE_KEY',        ' ;A6~>e_c*H|jjkmMu$R+:YzdU}x4=MNx7n>Tt3;5cn71jG,<<u=G28K3Wnz$}3L');
define('AUTH_SALT',        '3{I4U$aVAryY6V~|,bOd}/@bu602C9Le.^XdPd,PckQKXc(qWeMb7RH[W=uR*MwQ');
define('SECURE_AUTH_SALT', '|ey1J;xF@Ubhqb]UnTt87h ]x,0m-8[EYw46!~sC--iH+~E0BQ?>bW7?f>T[_c.V');
define('LOGGED_IN_SALT',   '~]A|9?3v]U}9`|:LSz>Q|YtM+$>#*R3o@o(Tx#G 5`VT&m^}er=,wN8abClK_?aK');
define('NONCE_SALT',       '#n>hQZ5NeQ%4M.GIH/(i@t(m/PoPp;GzFi_Q2BONZ7Zx_B0wFwYhq9-~J.jaq^mC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
